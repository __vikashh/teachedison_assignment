"""article URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.conf.urls.static import static
from article_feeds import views
urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', views.dashboard),
    path('<int:id>/like', views.like_article),
    path('<int:id>/dislike', views.dislike_article),
    path('<int:pk>/detail', views.ArticleDetailView.as_view(), name='article_detail'),
    path('signup/', views.SignUp.as_view()),
    path('article/', views.article_list),
    path('create/', views.article_create),
    path('<int:id>/edit', views.edit_article),
    path('manage/', views.manage_account),
    path('<int:id>/delete/', views.delete_article),
    path('<int:id>/like_dislike/', views.view_likes),
    path('<int:id>/block/', views.block_article),
]
if settings.DEBUG:
    urlpatterns +=static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
