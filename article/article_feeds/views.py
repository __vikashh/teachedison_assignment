from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator
from django.views.generic import ListView, DetailView, CreateView
from django.views.generic.edit import UpdateView, DeleteView
from article_feeds.models import Article, LikeDislike, CustomUser
from article_feeds.forms import (
    ArticleForm,
    CustomUserCreationForm,
    CustomUserChangeForm,
    EditUser
)


# Create your views here
@login_required
def dashboard(request):
    articles =  Article.objects.exclude(author=request.user).filter(
        categories__in = request.user.choices
    )
    paginator = Paginator(
         articles, 2
    )
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    context = {
        'article': articles,
        'page_obj': page_obj,
    }
    return render(request, 'article_feeds/dashboard.html', context)


# function to add like option for the user
@login_required
def like_article(request, id):
    """
    function will take id(primary key of article) as input
    """

    article = Article.objects.filter(id=id).first()
    if LikeDislike.objects.filter(
        article=article, user=request.user, like_dislike='l').exists():
        return redirect('/')

    elif LikeDislike.objects.filter(
        article=article, user=request.user, like_dislike='d').exists():
        LikeDislike.objects.filter(
            article=article, user=request.user).delete()
        l = LikeDislike.objects.create(
            article=article, user=request.user, like_dislike='l')
        l.save()
        return redirect('/')

    else:
        l = LikeDislike.objects.create(article=article, user=request.user,
                                   like_dislike='l' )
        l.save()
        return redirect('/')


# function to prvide dislike functionality to users
@login_required
def dislike_article(request, id):
    """
    function will take id(primary key of article) as input
    """

    article = Article.objects.filter(id=id).first()
    if LikeDislike.objects.filter(article=article,
                                  user=request.user,
                                  like_dislike='l').exists():
        LikeDislike.objects.filter(article=article,user=request.user).delete()
        l = LikeDislike.objects.create(article=article, user=request.user,
                                   like_dislike="d")
        l.save()
        return redirect('/')
    elif LikeDislike.objects.filter(article=article,
                                    user=request.user,
                                    like_dislike='d').exists():
        return redirect('/')
    else:
        l = LikeDislike.objects.create(article=article, user=request.user,
                                   like_dislike="d")
        l.save()
        return redirect('/')


@login_required
def block_article(request, id):
    article = Article.objects.filter(id=id).first()
    if LikeDislike.objects.filter(user=request.user,
                                  like_dislike="b").exists():
        return redirect('/')
    like_dislike = LikeDislike.objects.create(
        article=article,
        user=request.user,
        like_dislike='b'
    )
    like_dislike.save()
    return redirect('/')


@login_required
def article_create(request):
    if request.method == 'POST':
        article_form = ArticleForm(request.POST, request.FILES)
        if article_form.is_valid():
            form = article_form.save(commit=False)
            form.author = request.user
            form.save()
        return redirect('/')
    else:
        article_form = ArticleForm()
    context = {
        'form': article_form
    }
    return render(request, 'article_feeds/create_article.html', context)


@login_required
def edit_article(request, id):
    article = Article.objects.filter(id=id).first()
    if request.method == 'POST':
        article_form = ArticleForm(request.POST, instance=article)
        if article_form.is_valid():
            form = article_form.save(commit=False)
            form.author = request.user
            form.save()
        return redirect('/article')
    else:
        article_form = ArticleForm(instance=article)
    context = {
        'form': article_form
    }
    return render(request, 'article_feeds/edit_article.html', context)


@login_required
def delete_article(request, id):
    article = Article.objects.filter(id=id)
    article.delete()
    return redirect('/article')


@login_required
def view_likes(request, id):

    """
    function takes article id as input and returns count of number of like
    dislike and block,
    """

    article = Article.objects.filter(id=id)
    block = article.first().likedislike_set.filter(
        article=article.first(),
        like_dislike = 'b'
    )
    like = LikeDislike.objects.filter(
        article=article.first(),
        like_dislike="l"
    )
    dislike =  LikeDislike.objects.filter(
        article=article.first(),
        like_dislike="d"
    )
    paginator = Paginator(
         article, 1
    )
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    like_count = like.count()
    dislike_count = dislike.count()
    block_count = block.count()
    context = {
        'article': article,
        'like': like,
        'dislike': dislike,
        'bloc': block,
        'block_count': block_count,
        'like_count': like_count,
        'dislike_count': dislike_count,
        'page_obj': page_obj
    }
    return render(
        request, 'article_feeds/view_like_dislike_block.html',context
    )


@login_required
def manage_account(request):
    """
    Function can help user to edit their personal info
    """
    user = request.user
    if request.method =='POST':
       custom_user_change_form = EditUser(request.POST, instance=user)
       if custom_user_change_form.is_valid():
           custom_user_change_form.save()
           return redirect('/')
    else:
        custom_user_change_form = EditUser(instance=user)
    context = {
        'form': custom_user_change_form
    }
    return render(request, 'article_feeds/setting.html', context)


@login_required
def article_list(request):
    article = Article.objects.filter(author=request.user)
    paginator = Paginator(
         article, 3
    )
    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)

    context = {
        'article': article,
        'page_obj': page_obj
    }
    return render(request, 'article_feeds/article_list.html', context)


class ArticleDetailView(DetailView):
    model = Article
    template_name = 'article_feeds/article_detail_page.html'

class SignUp(CreateView):
    template_name = 'article_feeds/registration.html'
    form_class = CustomUserCreationForm
    success_url = '/accounts/login'
