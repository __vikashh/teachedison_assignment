from django.apps import AppConfig


class ArticleFeedsConfig(AppConfig):
    name = 'article_feeds'
