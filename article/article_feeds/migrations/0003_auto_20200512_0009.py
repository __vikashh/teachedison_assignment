# Generated by Django 2.1 on 2020-05-12 00:09

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('article_feeds', '0002_auto_20200511_1805'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='likedislike',
            name='dislike',
        ),
        migrations.RemoveField(
            model_name='likedislike',
            name='like',
        ),
        migrations.AddField(
            model_name='likedislike',
            name='like_dislike',
            field=models.CharField(default=1, max_length=10),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='likedislike',
            name='user',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='article',
            name='author',
            field=models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
        ),
    ]
