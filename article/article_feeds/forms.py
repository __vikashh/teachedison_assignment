from django import forms
from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Article, CustomUser


class DateInput(forms.DateInput):
    input_type = 'date'


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = CustomUser
        fields = ('first_name', 'last_name','dob', 'mobile',
                  'choices', 'username', 'email', )
        widgets = {
            'dob': DateInput(),
        }


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = CustomUser
        fields = ('username', 'email')

class EditUser(forms.ModelForm):
    class Meta:
        model = CustomUser
        exclude = [
            'password', 'last_login', 'is_active', 'is_staff',
            'last_login', 'is_superuser', 'groups', 'user_permissions',
            'last_login', 'date_joined'
        ]
        widgets = {
            'dob': DateInput(),
        }

class ArticleForm(forms.ModelForm):
    class Meta:
        model = Article
        fields = ('categories', 'title', 'body', 'image',)
