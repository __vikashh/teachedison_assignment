from django.db import models
from django.contrib.auth.models import AbstractUser
from multiselectfield import MultiSelectField
from phonenumber_field.modelfields import PhoneNumberField


# Create your models here.
class CustomUser(AbstractUser):
    MY_CHOICES = (
        ('Sport', 'Sport'),
        ('Politics', 'Politics'),
        ('Economics', 'Economics'),
        ('Science and technology', 'Science and technology'),
        ('Entertenment', 'Entertenment'),
    )
    choices = MultiSelectField(choices=MY_CHOICES)
    mobile = PhoneNumberField()
    dob = models.DateField(null=True, blank=True)

class Article(models.Model):
    MY_CHOICES = (
        ('Sport', 'Sport'),
        ('Politics', 'Politics'),
        ('Economics', 'Economics'),
        ('Science and technology', 'Science and technology'),
        ('Entertenment', 'Entertenment'),
    )
    author = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    categories = models.CharField(max_length=50, choices=MY_CHOICES)
    title = models.CharField(max_length=50)
    body = models.TextField()
    image = models.ImageField()
    date_created = models.DateField(auto_now=True)

class LikeDislike(models.Model):
    article = models.ForeignKey(Article, on_delete=models.CASCADE)
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    like_dislike = models.CharField(max_length=10)
