from django.contrib import admin
from article_feeds.models import Article, CustomUser, LikeDislike
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
# Register your models here.
class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser


admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Article,),
admin.site.register(LikeDislike,),
